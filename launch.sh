#!/usr/bin/env bash

export EDITOR=$(which kate)
export DOCTOR_TERMINAL_PATH="../repository/"

cd $(dirname $0)
mvn package
cd ./target/

java -jar ./DoctorTerminal.jar $@
