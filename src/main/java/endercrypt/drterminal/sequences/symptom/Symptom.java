package endercrypt.drterminal.sequences.symptom;


import endercrypt.drterminal.sequences.doctor.model.Specimen;
import endercrypt.drterminal.utility.editor.Editor;

import lombok.Getter;


@Getter
public class Symptom extends Specimen
{
	public static Symptom askUser()
	{
		return new Symptom(Editor.auto().query());
	}
	
	protected Symptom(String text)
	{
		super(text);
	}
	
	@Override
	public String toString()
	{
		return getText();
	}
}
