package endercrypt.drterminal.sequences.symptom;


import endercrypt.drterminal.sequences.ailment.model.AilmentHomeDirectory;


public class SampleSymptom extends Symptom
{
	public static SampleSymptom load(AilmentHomeDirectory homeDirectory)
	{
		return new SampleSymptom(homeDirectory.getSample().readAsString());
	}
	
	private SampleSymptom(String text)
	{
		super(text);
	}
}
