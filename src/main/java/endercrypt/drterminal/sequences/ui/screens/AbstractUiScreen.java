package endercrypt.drterminal.sequences.ui.screens;


import endercrypt.drterminal.sequences.ui.terminal.screen.AbstractShortLivedTerminalScreen;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public abstract class AbstractUiScreen extends AbstractShortLivedTerminalScreen implements UiScreen
{
	
}
