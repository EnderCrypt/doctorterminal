package endercrypt.drterminal.sequences.ui.screens.diagnosis;


import endercrypt.drterminal.exceptions.DoctorInFatalConditionException;
import endercrypt.drterminal.sequences.doctor.Diagnosis;
import endercrypt.drterminal.sequences.doctor.model.Disease;
import endercrypt.drterminal.sequences.scanners.result.ScannerResultPropertyInjector;
import endercrypt.drterminal.sequences.ui.graphics.SequentialTextPrinter;
import endercrypt.drterminal.sequences.ui.screens.AbstractUiScreen;
import endercrypt.drterminal.utility.ListTraveller;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

import lombok.Getter;


@Getter
public class DiagnosisScreen extends AbstractUiScreen
{
	private final Diagnosis diagnosis;
	private final ListTraveller<Disease> index;
	
	public DiagnosisScreen(Diagnosis diagnosis)
	{
		this.diagnosis = diagnosis;
		this.index = new ListTraveller<>(diagnosis.getAll());
	}
	
	@Override
	public void onKey(KeyStroke key)
	{
		if (key.getKeyType() == KeyType.ArrowLeft)
		{
			index.prev();
		}
		if (key.getKeyType() == KeyType.ArrowRight)
		{
			index.next();
		}
	}
	
	@Override
	public void onPaint(TextGraphics graphics) throws DoctorInFatalConditionException
	{
		SequentialTextPrinter printer = new SequentialTextPrinter(graphics);
		
		printer.setTextColor(TextColor.ANSI.WHITE);
		printer.text("Doctor Terminal");
		
		Disease disease = index.getItem();
		if (disease == null)
		{
			paintNegative(printer);
		}
		else
		{
			paintPositive(printer, disease);
		}
	}
	
	private void paintNegative(SequentialTextPrinter printer)
	{
		printer.setTextColor(TextColor.ANSI.BLACK);
		printer.textBanner(TextColor.ANSI.GREEN, "All clean!");
	}
	
	private void paintPositive(SequentialTextPrinter printer, Disease disease)
	{
		if (index.count() > 1)
		{
			printer.text("> Entry " + (index.getIndex() + 1) + " out of " + index.count() + " (Use left/right arrow keys to navigate)");
		}
		
		printer.setTextColor(TextColor.ANSI.BLACK);
		printer.textBanner(TextColor.ANSI.RED, "Detection!");
		printer.newline();
		
		ScannerResultPropertyInjector injector = new ScannerResultPropertyInjector(disease.getResults());
		
		printer.setTextColor(TextColor.ANSI.WHITE);
		printer.text("Disease:     " + disease.getAilment().getName());
		printer.text("Author:      " + disease.getAilment().getAuthor());
		printer.text("Context:     " + disease.getAilment().getContext());
		printer.text("Severity:    " + disease.getAilment().getSeverity());
		printer.text("Description: ");
		printer.intendentedMultilineText(2, injector.create(disease.getAilment().getDescription()));
		printer.newline();
		printer.text("Specimen:");
		printer.intendentedMultilineText(2, disease.getSpecimen().toString());
	}
}
