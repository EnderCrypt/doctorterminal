package endercrypt.drterminal.sequences.ui.terminal;


import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.ansi.UnixLikeTerminal.CtrlCBehaviour;


public class TweakedDefaultTerminalFactory extends DefaultTerminalFactory
{
	public TweakedDefaultTerminalFactory()
	{
		setUnixTerminalCtrlCBehaviour(CtrlCBehaviour.CTRL_C_KILLS_APPLICATION);
	}
}
