package endercrypt.drterminal.sequences.ui.terminal;


import endercrypt.drterminal.exceptions.DoctorInFatalConditionException;

import com.googlecode.lanterna.graphics.TextGraphics;


public interface PaintableTerminal
{
	public void onPaint(TextGraphics graphics) throws DoctorInFatalConditionException;
}
