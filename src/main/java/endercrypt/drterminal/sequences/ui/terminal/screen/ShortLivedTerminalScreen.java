package endercrypt.drterminal.sequences.ui.terminal.screen;


import endercrypt.drterminal.sequences.ui.terminal.PaintableTerminal;

import com.googlecode.lanterna.input.KeyStroke;


public interface ShortLivedTerminalScreen extends PaintableTerminal
{
	public boolean isAlive();
	
	public void onKey(KeyStroke key);
}
