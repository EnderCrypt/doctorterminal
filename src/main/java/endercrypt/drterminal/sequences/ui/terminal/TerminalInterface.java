package endercrypt.drterminal.sequences.ui.terminal;


import endercrypt.drterminal.exceptions.DoctorInFatalConditionException;
import endercrypt.drterminal.sequences.ui.terminal.screen.ShortLivedTerminalScreen;

import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JFrame;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.Screen.RefreshType;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalFactory;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class TerminalInterface implements AutoCloseable
{
	private static final TerminalFactory terminalFactory = new TweakedDefaultTerminalFactory();
	
	private final Terminal terminal;
	private final Screen screen;
	
	public TerminalInterface() throws DoctorInFatalConditionException
	{
		try
		{
			terminal = terminalFactory.createTerminal();
			if (terminal instanceof SwingTerminalFrame swingTerminal)
			{
				swingTerminal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				swingTerminal.setPreferredSize(new Dimension(1000, 400));
				swingTerminal.pack();
				swingTerminal.setLocationRelativeTo(null);
				swingTerminal.setVisible(true);
				swingTerminal.setLocationRelativeTo(null);
			}
			
			screen = new TerminalScreen(terminal);
			screen.startScreen();
			screen.setCursorPosition(null);
			
		}
		catch (IOException e)
		{
			throw new DoctorInFatalConditionException("Failed to startup the terminal", e);
		}
	}
	
	public KeyStroke readInput()
	{
		try
		{
			KeyStroke key = terminal.readInput();
			log.info("Receieved key stroke: " + key);
			return key;
		}
		catch (IOException e)
		{
			throw new DoctorInFatalConditionException("Failed to read key presses from terminal", e);
		}
	}
	
	public void render(ShortLivedTerminalScreen screen)
	{
		while (screen.isAlive())
		{
			redraw(screen);
			screen.onKey(readInput());
		}
	}
	
	public synchronized void redraw(PaintableTerminal paintableTerminal) throws DoctorInFatalConditionException
	{
		// draw
		TerminalSize newSize = screen.doResizeIfNecessary();
		if (newSize != null)
		{
			log.info("Terminal size changed to " + newSize);
		}
		
		// reset
		screen.clear();
		screen.setCursorPosition(null);
		
		// draw
		paintableTerminal.onPaint(screen.newTextGraphics());
		
		// refresh
		try
		{
			screen.refresh(RefreshType.COMPLETE);
		}
		catch (IOException e)
		{
			throw new DoctorInFatalConditionException("Failed to refresh the terminal", e);
		}
	}
	
	@Override
	public void close() throws DoctorInFatalConditionException
	{
		try
		{
			screen.stopScreen();
		}
		catch (IOException e)
		{
			throw new DoctorInFatalConditionException("Failed to stop the terminal", e);
		}
	}
	
	public static void disgracefulClose(TerminalInterface terminalLink)
	{
		try
		{
			if (terminalLink != null)
			{
				terminalLink.close();
			}
		}
		catch (DoctorInFatalConditionException e)
		{
			// disgraceful ignore
		}
	}
}
