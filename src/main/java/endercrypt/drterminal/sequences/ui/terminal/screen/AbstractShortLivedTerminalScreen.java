package endercrypt.drterminal.sequences.ui.terminal.screen;


import com.googlecode.lanterna.input.KeyStroke;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;


@Log4j2
@Getter
public abstract class AbstractShortLivedTerminalScreen implements ShortLivedTerminalScreen
{
	private boolean alive = true;
	
	public void kill()
	{
		if (isAlive())
		{
			alive = false;
			log.info("Menu " + this + " deactivated");
		}
	}
	
	@Override
	public abstract void onKey(KeyStroke key);
}
