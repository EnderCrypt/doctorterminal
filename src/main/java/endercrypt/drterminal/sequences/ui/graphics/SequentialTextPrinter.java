package endercrypt.drterminal.sequences.ui.graphics;


import endercrypt.drterminal.utility.splitter.TextSplitter;

import java.util.List;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Getter
@RequiredArgsConstructor
public class SequentialTextPrinter
{
	private int row = 0;
	private final TextGraphics graphics;
	
	@Setter
	private TextColor textColor = TextColor.ANSI.WHITE;
	
	public void newline()
	{
		row++;
	}
	
	public void textBanner(TextColor background, String text)
	{
		graphics.setBackgroundColor(background);
		for (int column = 0; column < graphics.getSize().getColumns(); column++)
		{
			graphics.setCharacter(column, row, ' ');
		}
		centeredText(text);
		graphics.setBackgroundColor(TextColor.ANSI.BLACK);
	}
	
	public void text(String text)
	{
		intendentedText(0, text);
	}
	
	public void intendentedMultilineText(int intendation, String text)
	{
		int columnsAvailable = graphics.getSize().getColumns() - intendation;
		if (columnsAvailable <= 0)
		{
			throw new IllegalArgumentException("available width is " + columnsAvailable);
		}
		List<String> lines = TextSplitter
			.createStandard(text)
			.stream(columnsAvailable)
			.toList();
		
		for (String line : lines)
		{
			intendentedText(intendation, line);
		}
	}
	
	public void centeredText(String text)
	{
		int column = (graphics.getSize().getColumns() / 2) - (text.length() / 2);
		intendentedMultilineText(column, text);
	}
	
	public void intendentedText(int intendation, String text)
	{
		graphics.setForegroundColor(getTextColor());
		graphics.putString(intendation, row, text);
		newline();
	}
}
