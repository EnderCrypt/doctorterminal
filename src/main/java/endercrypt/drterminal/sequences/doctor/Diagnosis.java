package endercrypt.drterminal.sequences.doctor;


import endercrypt.drterminal.sequences.doctor.model.Disease;
import endercrypt.drterminal.sequences.symptom.Symptom;
import endercrypt.drterminal.utility.data.AbstractPublicallyImmutableDataList;

import java.util.Comparator;
import java.util.List;

import lombok.Getter;


@Getter
public class Diagnosis extends AbstractPublicallyImmutableDataList<Disease>
{
	private final Symptom symptom;
	
	protected Diagnosis(Symptom symptom, List<Disease> diseases)
	{
		this.symptom = symptom;
		
		diseases
			.stream()
			.sorted(new DiseaseSeveritySorter())
			.forEach(elements::add);
	}
	
	private static class DiseaseSeveritySorter implements Comparator<Disease>
	{
		@Override
		public int compare(Disease disease1, Disease disease2)
		{
			int severity1 = disease1.getAilment().getSeverity().ordinal();
			int severity2 = disease2.getAilment().getSeverity().ordinal();
			return Integer.compare(severity1, severity2);
		}
	}
}
