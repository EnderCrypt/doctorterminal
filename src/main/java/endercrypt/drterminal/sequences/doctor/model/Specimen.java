package endercrypt.drterminal.sequences.doctor.model;


import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class Specimen
{
	private final String text;
	
	@Override
	public String toString()
	{
		return getText();
	}
}
