package endercrypt.drterminal.sequences.doctor.model;

public interface Cure
{
	public void perform();
}
