package endercrypt.drterminal.sequences.doctor.model;


import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.scanners.result.ScannerResults;

import java.util.Optional;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Disease
{
	public static Optional<Disease> construct(ScannerResults results)
	{
		return results.getSpecimen().getValue()
			.map(specimen -> new Disease(results.getAilment(), specimen, results));
	}
	
	private final Ailment ailment;
	
	private final Specimen specimen;
	
	private final ScannerResults results;
}
