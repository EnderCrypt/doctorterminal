package endercrypt.drterminal.sequences.doctor;


import endercrypt.drterminal.sequences.ailment.repository.AilmentRepository;
import endercrypt.drterminal.sequences.doctor.model.Disease;
import endercrypt.drterminal.sequences.scanners.model.scanner.AilmentScanner;
import endercrypt.drterminal.sequences.symptom.Symptom;

import java.util.List;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;


@Log4j2
@RequiredArgsConstructor
public class Doctor
{
	private final AilmentRepository repository;
	
	public Diagnosis diagnose(Symptom symptom)
	{
		List<Disease> diseases = repository.stream()
			.map(ailment -> AilmentScanner.scan(ailment, symptom))
			.flatMap(result -> Disease.construct(result).stream())
			.collect(Collectors.toList());
		
		log.info("Diseases:\n" + diseases
			.stream()
			.map(result -> "- " + result.getAilment().getName())
			.collect(Collectors.joining("\n")));
		
		return new Diagnosis(symptom, diseases);
	}
}
