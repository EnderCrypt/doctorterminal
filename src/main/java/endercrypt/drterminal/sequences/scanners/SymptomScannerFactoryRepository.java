package endercrypt.drterminal.sequences.scanners;


import endercrypt.drterminal.sequences.scanners.model.factory.AilmentScannerFactory;
import endercrypt.drterminal.sequences.scanners.model.type.SymptomScannerType;
import endercrypt.drterminal.sequences.scanners.types.text.plain.TextPlainAilmentScannerFactory;
import endercrypt.drterminal.sequences.scanners.types.text.regex.TextRegexAilmentScannerFactory;
import endercrypt.drterminal.utility.data.AbstractPublicallyImmutableDataSet;

import java.util.Optional;


public class SymptomScannerFactoryRepository extends AbstractPublicallyImmutableDataSet<AilmentScannerFactory>
{
	public SymptomScannerFactoryRepository()
	{
		elements.add(new TextPlainAilmentScannerFactory());
		elements.add(new TextRegexAilmentScannerFactory());
	}
	
	public Optional<AilmentScannerFactory> get(SymptomScannerType scholarType)
	{
		return stream()
			.filter(scholar -> scholar.getType().equals(scholarType))
			.findFirst();
	}
}
