package endercrypt.drterminal.sequences.scanners.result;


import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class ScannerResultPropertyInjector
{
	private static final Pattern parseVariablePattern = Pattern.compile("\\{([a-zA-Z0-9_]+)\\}");
	
	@NonNull
	private final ScannerResultProperties properties;
	
	private String generateParseVariable(MatchResult match)
	{
		return properties.getAsString(match.group(1));
	}
	
	public String create(String text)
	{
		Matcher matcher = parseVariablePattern.matcher(text);
		while (matcher.find())
		{
			text = matcher.replaceAll(this::generateParseVariable);
		}
		return text;
	}
}
