package endercrypt.drterminal.sequences.scanners.result;


import java.util.Optional;

import lombok.Getter;
import lombok.Setter;


public class ScannerResultProperty<T> implements RenderableProperty
{
	@Getter
	private final String name;
	
	@Setter
	private T value;
	
	protected ScannerResultProperty(String name)
	{
		this.name = name;
	}
	
	public Optional<T> getValue()
	{
		return Optional.ofNullable(value);
	}
	
	@Override
	public String renderAsString()
	{
		T value = getValue().orElse(null);
		if (value == null)
		{
			return createMissingPropertyText(getName());
		}
		if (value instanceof RenderableProperty renderable)
		{
			return renderable.renderAsString();
		}
		return value.toString();
	}
	
	@Override
	public String toString()
	{
		return getName() + " -> " + getValue().orElse(null);
	}
	
	public static String createMissingPropertyText(String name)
	{
		return "[MISSING -> \"" + name + "\" <- MISSING]";
	}
}
