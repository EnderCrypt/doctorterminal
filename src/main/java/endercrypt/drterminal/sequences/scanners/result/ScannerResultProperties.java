package endercrypt.drterminal.sequences.scanners.result;


import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;


public class ScannerResultProperties
{
	private final Map<String, AttachedScannerResultProperty<?>> properties = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
	
	public <T> ScannerResultProperty<T> createProperty(String name, T value)
	{
		ScannerResultProperty<T> property = createProperty(new ScannerResultProperty<>(name));
		property.setValue(value);
		return property;
	}
	
	public <T> ScannerResultProperty<T> createProperty(ScannerResultProperty<T> property)
	{
		addProperty(new AttachedScannerResultProperty<>(this, property));
		return property;
	}
	
	void addProperty(AttachedScannerResultProperty<?> attachedProperty)
	{
		if (attachedProperty.getParent() != this)
		{
			throw new IllegalArgumentException("Incompatible scanner property: " + attachedProperty);
		}
		ScannerResultProperty<?> otherProperty = get(attachedProperty.getProperty().getName()).orElse(null);
		if (otherProperty != null && attachedProperty.getProperty() != otherProperty)
		{
			throw new IllegalArgumentException("The property " + attachedProperty.getProperty().getName() + " was expected to be attached, but wasnt");
		}
		properties.put(attachedProperty.getProperty().getName(), attachedProperty);
	}
	
	public <T> Optional<ScannerResultProperty<T>> get(String name)
	{
		return Optional.ofNullable((AttachedScannerResultProperty<T>) properties.get(name))
			.map(AttachedScannerResultProperty::getProperty);
	}
	
	public String getAsString(String name)
	{
		return get(name)
			.map(ScannerResultProperty::renderAsString)
			.orElseGet(() -> ScannerResultProperty.createMissingPropertyText(name));
	}
}
