package endercrypt.drterminal.sequences.scanners.result;


import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.doctor.model.Cure;
import endercrypt.drterminal.sequences.doctor.model.Specimen;

import lombok.Getter;
import lombok.NonNull;


@Getter
public class ScannerResults extends ScannerResultProperties
{
	@NonNull
	private final Ailment ailment;
	
	private final ScannerResultProperty<Specimen> specimen = createProperty(new ScannerResultProperty<>("specimen"));
	
	private final ScannerResultProperty<Cure> cure = createProperty(new ScannerResultProperty<>("cure"));
	
	public ScannerResults(@NonNull Ailment ailment)
	{
		this.ailment = ailment;
		createProperty("name", ailment.getName());
		createProperty("author", ailment.getAuthor());
		createProperty("context", ailment.getContext());
		createProperty("severity", ailment.getSeverity());
		createProperty("type", ailment.getType());
		createProperty("description", ailment.getDescription());
	}
	
	@Override
	public String toString()
	{
		return ScannerResults.class.getSimpleName() + "[ailment=" + getAilment() + ", diseased=" + getSpecimen().getValue().isPresent() + ", curable=" + getCure().getValue().isPresent() + "]";
	}
}
