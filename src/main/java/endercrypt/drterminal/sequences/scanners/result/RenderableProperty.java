package endercrypt.drterminal.sequences.scanners.result;

public interface RenderableProperty
{
	public String renderAsString();
}
