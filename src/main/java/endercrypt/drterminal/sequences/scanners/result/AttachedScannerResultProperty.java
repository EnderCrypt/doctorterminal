package endercrypt.drterminal.sequences.scanners.result;


import lombok.Getter;
import lombok.NonNull;


@Getter
public class AttachedScannerResultProperty<T>
{
	private final ScannerResultProperties parent;
	
	private final ScannerResultProperty<T> property;
	
	public AttachedScannerResultProperty(@NonNull ScannerResultProperties parent, @NonNull ScannerResultProperty<T> property)
	{
		this.parent = parent;
		this.property = property;
	}
	
	@Override
	public String toString()
	{
		return property.toString();
	}
}
