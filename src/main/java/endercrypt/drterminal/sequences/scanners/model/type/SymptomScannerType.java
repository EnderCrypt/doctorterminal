package endercrypt.drterminal.sequences.scanners.model.type;


import endercrypt.drterminal.exceptions.DoctorInCriticalConditionException;
import endercrypt.drterminal.sequences.scanners.SymptomScannerFactoryRepository;
import endercrypt.drterminal.sequences.scanners.model.factory.AilmentScannerFactory;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.EqualsAndHashCode;


@EqualsAndHashCode
public class SymptomScannerType
{
	public static final String SEPARATOR = "/";
	
	public static SymptomScannerType parse(String string)
	{
		List<String> names = Arrays.asList(string.split(SEPARATOR));
		
		return new SymptomScannerType(names);
	}
	
	private final Set<String> names = new LinkedHashSet<>();
	
	public SymptomScannerType(String... names)
	{
		this(Arrays.asList(names));
	}
	
	public SymptomScannerType(List<String> names)
	{
		names
			.stream()
			.map(String::trim)
			.map(String::toLowerCase)
			.forEach(this.names::add);
	}
	
	public AilmentScannerFactory loadFromRepository(SymptomScannerFactoryRepository repository)
	{
		return repository.get(this)
			.orElseThrow(() -> new DoctorInCriticalConditionException("Missing symptom scanner type: " + this));
	}
	
	@Override
	public String toString()
	{
		return names
			.stream()
			.collect(Collectors.joining(SEPARATOR));
	}
}
