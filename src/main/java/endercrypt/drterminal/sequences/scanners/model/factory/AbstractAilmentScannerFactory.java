package endercrypt.drterminal.sequences.scanners.model.factory;


import endercrypt.drterminal.sequences.scanners.model.type.SymptomScannerType;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public abstract class AbstractAilmentScannerFactory implements AilmentScannerFactory
{
	private final SymptomScannerType type;
}
