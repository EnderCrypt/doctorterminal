package endercrypt.drterminal.sequences.scanners.model.factory;


import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.scanners.model.scanner.AilmentScanner;
import endercrypt.drterminal.sequences.scanners.model.type.SymptomScannerType;


public interface AilmentScannerFactory
{
	public SymptomScannerType getType();
	
	public AilmentScanner createAilmentScanner(Ailment ailment);
}
