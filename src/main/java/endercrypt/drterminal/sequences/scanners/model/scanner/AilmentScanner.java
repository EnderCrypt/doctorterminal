package endercrypt.drterminal.sequences.scanners.model.scanner;


import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.scanners.result.ScannerResults;
import endercrypt.drterminal.sequences.symptom.Symptom;


public interface AilmentScanner
{
	public static ScannerResults scan(Ailment ailment, Symptom symptom)
	{
		ScannerResults results = new ScannerResults(ailment);
		ailment.getScanner().scan(symptom, results);
		return results;
	}
	
	public void scan(Symptom symptom, ScannerResults results);
}
