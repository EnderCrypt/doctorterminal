package endercrypt.drterminal.sequences.scanners.model.scanner;


import endercrypt.drterminal.sequences.ailment.model.Ailment;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public abstract class AbstractAilmentScanner implements AilmentScanner
{
	private final Ailment ailment;
}
