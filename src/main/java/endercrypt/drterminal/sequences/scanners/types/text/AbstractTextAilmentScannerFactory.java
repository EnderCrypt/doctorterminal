package endercrypt.drterminal.sequences.scanners.types.text;


import endercrypt.drterminal.sequences.scanners.model.factory.AbstractAilmentScannerFactory;
import endercrypt.drterminal.sequences.scanners.model.type.SymptomScannerType;


public abstract class AbstractTextAilmentScannerFactory extends AbstractAilmentScannerFactory
{
	public AbstractTextAilmentScannerFactory(SymptomScannerType type)
	{
		super(type);
	}
}
