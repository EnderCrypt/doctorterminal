package endercrypt.drterminal.sequences.scanners.types.text.plain;


import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.scanners.model.scanner.AilmentScanner;
import endercrypt.drterminal.sequences.scanners.model.type.SymptomScannerType;
import endercrypt.drterminal.sequences.scanners.types.text.AbstractTextAilmentScannerFactory;


public class TextPlainAilmentScannerFactory extends AbstractTextAilmentScannerFactory
{
	public TextPlainAilmentScannerFactory()
	{
		super(new SymptomScannerType("text", "plain"));
	}
	
	@Override
	public AilmentScanner createAilmentScanner(Ailment ailment)
	{
		return new TextPlainAilmentScanner(ailment);
	}
}
