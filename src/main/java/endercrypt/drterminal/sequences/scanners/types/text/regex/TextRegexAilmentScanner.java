package endercrypt.drterminal.sequences.scanners.types.text.regex;


import endercrypt.drterminal.sequences.ailment.directories.AilmentFile.ReadAttribute;
import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.doctor.model.Specimen;
import endercrypt.drterminal.sequences.scanners.result.ScannerResults;
import endercrypt.drterminal.sequences.scanners.types.text.AbstractTextAilmentScanner;
import endercrypt.drterminal.sequences.symptom.Symptom;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TextRegexAilmentScanner extends AbstractTextAilmentScanner
{
	private final Pattern pattern;
	
	public TextRegexAilmentScanner(Ailment ailment)
	{
		super(ailment);
		String signature = ailment.getHomeDirectory().getSignature().readAsString(ReadAttribute.NO_TAILING_NEWLINE);
		this.pattern = Pattern.compile(signature, Pattern.MULTILINE);
	}
	
	@Override
	public void scan(Symptom symptom, ScannerResults results)
	{
		Matcher matcher = pattern.matcher(symptom.getText());
		if (matcher.find())
		{
			results.getSpecimen().setValue(new Specimen(matcher.group(0)));
			for (int groupIndex = 1; groupIndex <= matcher.groupCount(); groupIndex++)
			{
				String text = matcher.group(groupIndex);
				Specimen specimen = new Specimen(text);
				results.createProperty("regex_group_" + groupIndex, specimen);
			}
		}
	}
}
