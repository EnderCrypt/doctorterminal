package endercrypt.drterminal.sequences.scanners.types.text.regex;


import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.scanners.model.scanner.AilmentScanner;
import endercrypt.drterminal.sequences.scanners.model.type.SymptomScannerType;
import endercrypt.drterminal.sequences.scanners.types.text.AbstractTextAilmentScannerFactory;


public class TextRegexAilmentScannerFactory extends AbstractTextAilmentScannerFactory
{
	public TextRegexAilmentScannerFactory()
	{
		super(new SymptomScannerType("text", "regex"));
	}
	
	@Override
	public AilmentScanner createAilmentScanner(Ailment ailment)
	{
		return new TextRegexAilmentScanner(ailment);
	}
}
