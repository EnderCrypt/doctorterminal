package endercrypt.drterminal.sequences.scanners.types.text;


import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.scanners.model.scanner.AbstractAilmentScanner;


public abstract class AbstractTextAilmentScanner extends AbstractAilmentScanner
{
	public AbstractTextAilmentScanner(Ailment ailment)
	{
		super(ailment);
	}
}
