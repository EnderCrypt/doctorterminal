package endercrypt.drterminal.sequences.scanners.types.text.plain;


import endercrypt.drterminal.sequences.ailment.directories.AilmentFile.ReadAttribute;
import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.doctor.model.Specimen;
import endercrypt.drterminal.sequences.scanners.result.ScannerResults;
import endercrypt.drterminal.sequences.scanners.types.text.AbstractTextAilmentScanner;
import endercrypt.drterminal.sequences.symptom.Symptom;


public class TextPlainAilmentScanner extends AbstractTextAilmentScanner
{
	private final String signature;
	
	public TextPlainAilmentScanner(Ailment ailment)
	{
		super(ailment);
		this.signature = ailment.getHomeDirectory().getSignature().readAsString(ReadAttribute.NO_TAILING_NEWLINE);
	}
	
	@Override
	public void scan(Symptom symptom, ScannerResults results)
	{
		if (symptom.getText().contains(signature))
		{
			results.getSpecimen().setValue(new Specimen(signature));
		}
	}
}
