package endercrypt.drterminal.sequences.ailment.directories;


import endercrypt.drterminal.exceptions.DoctorInFatalConditionException;
import endercrypt.drterminal.sequences.ailment.model.AilmentHomeDirectory;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class AilmentCollector implements FileVisitor<Path>
{
	public static Set<AilmentHomeDirectory> perform(Path path) throws DoctorInFatalConditionException
	{
		AilmentCollector collector = new AilmentCollector();
		try
		{
			Files.walkFileTree(path, collector);
		}
		catch (IOException e)
		{
			throw new DoctorInFatalConditionException("Failed to collect all ailments from " + path, e);
		}
		return collector.getAilmentPaths();
	}
	
	private final Set<AilmentHomeDirectory> directories = new HashSet<>();
	
	public Set<AilmentHomeDirectory> getAilmentPaths()
	{
		return Collections.unmodifiableSet(directories);
	}
	
	@Override
	public FileVisitResult preVisitDirectory(Path directory, BasicFileAttributes attributes) throws IOException
	{
		log.trace("Visit: " + directory);
		AilmentHomeDirectory homeDirectory = new AilmentHomeDirectory(directory);
		
		if (homeDirectory.isValid() == false)
		{
			return FileVisitResult.CONTINUE;
		}
		directories.add(homeDirectory);
		return FileVisitResult.SKIP_SUBTREE;
	}
	
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException
	{
		return FileVisitResult.CONTINUE;
	}
	
	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exception) throws IOException
	{
		return FileVisitResult.CONTINUE;
	}
	
	@Override
	public FileVisitResult postVisitDirectory(Path directory, IOException exception) throws IOException
	{
		return FileVisitResult.CONTINUE;
	}
}
