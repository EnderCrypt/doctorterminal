package endercrypt.drterminal.sequences.ailment.directories;


import endercrypt.drterminal.utility.data.AbstractPublicallyImmutableDataSet;
import endercrypt.library.commons.misc.Ender;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Predicate;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class AilmentDirectories extends AbstractPublicallyImmutableDataSet<AilmentDirectory>
{
	public AilmentDirectories()
	{
		// default
		add(Ender.info.jar.directory.resolve("repository"));
		
		// from ENV variables
		Arrays.stream(Optional.ofNullable(System.getenv("DOCTOR_TERMINAL_PATH")).orElse("").split(":"))
			.map(String::trim)
			.filter(Predicate.not(String::isBlank))
			.map(Ender.info.workingDirectory::resolve)
			.forEach(this::add);
	}
	
	public void add(String path)
	{
		add(Paths.get(path));
	}
	
	public void add(Path path)
	{
		add(new AilmentDirectory(path));
	}
	
	public void add(AilmentDirectory directory)
	{
		if (elements.contains(directory) == false)
		{
			elements.add(directory);
			log.debug("Added path: " + directory);
		}
	}
}
