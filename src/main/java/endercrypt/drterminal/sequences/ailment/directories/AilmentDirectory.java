package endercrypt.drterminal.sequences.ailment.directories;


import endercrypt.drterminal.exceptions.DoctorInFatalConditionException;
import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.ailment.model.AilmentFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;


@Getter
@Log4j2
public class AilmentDirectory
{
	private final Path path;
	
	public AilmentDirectory(@NonNull Path path)
	{
		this.path = path;
	}
	
	public Set<Ailment> collectAilments(AilmentFactory factory) throws DoctorInFatalConditionException
	{
		if (Files.exists(getPath()) == false || Files.isDirectory(getPath()) == false)
		{
			log.warn("Failed to collect from invalid path: " + getPath());
			return Set.of();
		}
		
		return AilmentCollector.perform(getPath())
			.stream()
			.map(factory::construct)
			.collect(Collectors.toUnmodifiableSet());
	}
	
	@Override
	public String toString()
	{
		return path.toString();
	}
}
