package endercrypt.drterminal.sequences.ailment.directories;


import endercrypt.drterminal.exceptions.DoctorInFatalConditionException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class AilmentFile
{
	private final Path path;
	
	public String readAsString()
	{
		try
		{
			return Files.readString(path);
		}
		catch (IOException e)
		{
			throw new DoctorInFatalConditionException("Failed to read " + path, e);
		}
	}
	
	public String readAsString(ReadAttribute... attributes)
	{
		StringBuilder builder = new StringBuilder(readAsString());
		
		Arrays.stream(attributes)
			.map(ReadAttribute::getProcessor)
			.forEach(p -> p.process(builder));
		
		return builder.toString();
	}
	
	@Override
	public String toString()
	{
		return getPath().toString();
	}
	
	@Getter
	@RequiredArgsConstructor
	public enum ReadAttribute
	{
		NO_TAILING_NEWLINE((builder) -> {
			while (builder.charAt(builder.length() - 1) == '\n')
			{
				builder.setLength(builder.length() - 1);
			}
		});
		
		private final ReadAttributeProcessor processor;
	}
	
	private interface ReadAttributeProcessor
	{
		public void process(StringBuilder builder);
	}
}
