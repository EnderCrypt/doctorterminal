package endercrypt.drterminal.sequences.ailment.model;


import endercrypt.drterminal.sequences.scanners.SymptomScannerFactoryRepository;

import com.google.gson.Gson;
import com.google.gson.JsonObject;


public class AilmentFactory
{
	private static final Gson gson = new Gson();
	
	private final SymptomScannerFactoryRepository scanners = new SymptomScannerFactoryRepository();
	
	public Ailment construct(AilmentHomeDirectory directory)
	{
		if (directory.isValid() == false)
		{
			throw new IllegalArgumentException(directory + " is an invalid ailment directory");
		}
		String rawJson = directory.getMetadata().readAsString();
		JsonObject rootJson = gson.fromJson(rawJson, JsonObject.class);
		return new Ailment(directory, rootJson, scanners);
	}
	
	@Override
	public String toString()
	{
		return scanners.toString();
	}
}
