package endercrypt.drterminal.sequences.ailment.model;


import endercrypt.drterminal.sequences.scanners.SymptomScannerFactoryRepository;
import endercrypt.drterminal.sequences.scanners.model.scanner.AilmentScanner;
import endercrypt.drterminal.sequences.scanners.model.type.SymptomScannerType;
import endercrypt.drterminal.sequences.symptom.SampleSymptom;

import com.google.gson.JsonObject;

import lombok.Getter;


@Getter
public class Ailment
{
	private final AilmentHomeDirectory homeDirectory;
	
	private final String name;
	private final String author;
	private final String context;
	private final SeverityGrade severity;
	private final SymptomScannerType type;
	private final String description;
	
	private final SampleSymptom sample;
	private final AilmentScanner scanner;
	
	protected Ailment(AilmentHomeDirectory homeDirectory, JsonObject root, SymptomScannerFactoryRepository scanners)
	{
		this.homeDirectory = homeDirectory;
		
		this.name = root.get("name").getAsString();
		this.author = root.get("author").getAsString();
		this.context = root.get("context").getAsString();
		this.severity = SeverityGrade.parse(root.get("severity").getAsString());
		this.type = SymptomScannerType.parse(root.get("type").getAsString());
		this.description = root.get("description").getAsString();
		
		this.scanner = type.loadFromRepository(scanners).createAilmentScanner(this);
		this.sample = SampleSymptom.load(homeDirectory);
	}
	
	@Override
	public String toString()
	{
		return getName();
	}
}
