package endercrypt.drterminal.sequences.ailment.model;


import endercrypt.drterminal.sequences.ailment.directories.AilmentFile;

import java.nio.file.Files;
import java.nio.file.Path;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class AilmentHomeDirectory
{
	@NonNull
	private final Path root;
	
	public boolean isValid()
	{
		return Files.isRegularFile(getMetadata().getPath());
	}
	
	public AilmentFile getMetadata()
	{
		return new AilmentFile(getRoot().resolve("./ailment.drt"));
	}
	
	public AilmentFile getSample()
	{
		return new AilmentFile(getRoot().resolve("./sample.drt"));
	}
	
	public AilmentFile getSignature()
	{
		return new AilmentFile(getRoot().resolve("./signature.txt"));
	}
}
