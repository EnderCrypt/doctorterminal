package endercrypt.drterminal.sequences.ailment.model;


import endercrypt.drterminal.exceptions.DoctorInFatalConditionException;
import endercrypt.library.commons.misc.Ender;


public enum SeverityGrade
{
	SAFE, // something thats a complete false positive and not worh worrying about
	MINOR, // minor issue, usually self inflicted, like trying to apt install wrong package name
	ISSUE, // potentially serious issue
	DANGEROUS; // something possibly system breaking or dangerous
	
	public static SeverityGrade parse(String text)
	{
		return Ender.parse.toEnum(SeverityGrade.class, false, text)
			.orElseThrow(() -> new DoctorInFatalConditionException("Unknown severety level: " + text));
	}
}
