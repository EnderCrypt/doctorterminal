package endercrypt.drterminal.sequences.ailment.repository;


import endercrypt.drterminal.exceptions.DoctorInFatalConditionException;
import endercrypt.drterminal.sequences.ailment.directories.AilmentDirectories;
import endercrypt.drterminal.sequences.ailment.directories.AilmentDirectory;
import endercrypt.drterminal.sequences.ailment.model.Ailment;
import endercrypt.drterminal.sequences.ailment.model.AilmentFactory;
import endercrypt.drterminal.utility.data.AbstractPublicallyImmutableDataSet;

import java.util.Set;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class AilmentRepository extends AbstractPublicallyImmutableDataSet<Ailment>
{
	private final AilmentFactory factory;
	
	public AilmentRepository(AilmentFactory factory, AilmentDirectories directories)
	{
		this.factory = factory;
		for (AilmentDirectory directory : directories)
		{
			add(directory);
		}
	}
	
	private void add(AilmentDirectory directory) throws DoctorInFatalConditionException
	{
		Set<Ailment> newAilments = directory.collectAilments(factory);
		log.info("Loaded new ailments: " + newAilments + " from " + directory);
		elements.addAll(newAilments);
	}
}
