package endercrypt.drterminal.arguments;


import endercrypt.library.groundwork.command.StandardArguments;

import lombok.Getter;
import picocli.CommandLine.Command;


@Getter
@Command(mixinStandardHelpOptions = true, helpCommand = true)
public class MainArguments extends StandardArguments
{
	
}
