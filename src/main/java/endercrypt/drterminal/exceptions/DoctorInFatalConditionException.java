package endercrypt.drterminal.exceptions;

/**
 * Inidicates that something has went horribly wrong and the application will
 * exit immediately
 * 
 * @author endercrypt
 */
public class DoctorInFatalConditionException extends RuntimeException
{
	private static final long serialVersionUID = -541559210927315616L;
	
	public DoctorInFatalConditionException(String message)
	{
		super(message);
	}
	
	public DoctorInFatalConditionException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
