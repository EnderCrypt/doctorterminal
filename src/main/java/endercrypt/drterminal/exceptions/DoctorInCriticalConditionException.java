package endercrypt.drterminal.exceptions;

/**
 * Indicates that a predicted but critical issue has arisen and the application
 * will exit immediately
 * 
 * @author endercrypt
 */
public class DoctorInCriticalConditionException extends DoctorInFatalConditionException
{
	private static final long serialVersionUID = 3901975503298520071L;
	
	public DoctorInCriticalConditionException(String message)
	{
		super(message);
	}
}
