package endercrypt.drterminal.utility.editor;


import endercrypt.drterminal.exceptions.DoctorInCriticalConditionException;
import endercrypt.drterminal.utility.data.AbstractPublicallyImmutableDataList;
import endercrypt.library.commons.misc.Ender;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


public class KnownEditors extends AbstractPublicallyImmutableDataList<Path>
{
	public KnownEditors()
	{
		// add editor via env variable
		Optional.ofNullable(System.getenv("EDITOR")).ifPresent(this::pursue);
		
		// add known editors
		Arrays.stream(KnownEditors.Known.values())
			.map(Known::getFilename)
			.forEach(this::pursue);
	}
	
	public boolean pursue(@NonNull String name)
	{
		Path directPath = Paths.get(name);
		if (Files.isExecutable(directPath))
		{
			elements.add(directPath);
			return true;
		}
		
		Path path = Ender.files.locateExecutable(name).orElse(null);
		if (path == null)
		{
			return false;
		}
		
		elements.add(path);
		return true;
	}
	
	public Path getBest()
	{
		return stream()
			.findFirst()
			.orElseThrow(() -> new DoctorInCriticalConditionException("No known editors found, set one using the $EDITOR enviromental variable"));
	}
	
	@Getter
	@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
	public enum Known
	{
		MICRO,
		NANO,
		NVIM,
		VIM,
		EMACS;
		
		private final String filename;
		
		private Known()
		{
			this.filename = name().toLowerCase();
		}
	}
}
