package endercrypt.drterminal.utility.editor;


import endercrypt.drterminal.exceptions.DoctorInFatalConditionException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;


@Log4j2
@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Editor
{
	public static Editor auto()
	{
		KnownEditors editors = new KnownEditors();
		return new Editor(editors.getBest());
	}
	
	private final Path executableFilePath;
	private final Path queryFilePath;
	
	private Editor(Path path)
	{
		this.executableFilePath = path;
		log.info("Using editor: " + getExecutableFilePath());
		try
		{
			queryFilePath = Files.createTempFile("enter_your_symptoms_", ".txt");
			log.debug("temporary query file: " + getQueryFilePath());
		}
		catch (IOException e)
		{
			throw new DoctorInFatalConditionException("Failed to create temporary file", e);
		}
	}
	
	public String query()
	{
		clearQueryFile();
		try
		{
			ProcessBuilder builder = new ProcessBuilder(getExecutableFilePath().toString(), getQueryFilePath().toString());
			builder.inheritIO();
			Process executableFilePath = builder.start();
			executableFilePath.waitFor();
			return readQueryFile();
		}
		catch (InterruptedException e)
		{
			throw new DoctorInFatalConditionException("Thread unexpectedly interrupted", e);
		}
		catch (IOException e)
		{
			throw new DoctorInFatalConditionException("Failed to query for symptom using editor " + getExecutableFilePath(), e);
		}
		finally
		{
			clearQueryFile();
		}
	}
	
	private String readQueryFile()
	{
		try
		{
			String contents = Files.readString(getQueryFilePath());
			log.info("Receieved " + contents.getBytes().length + " bytes from editor");
			log.trace("Data:\n" + contents);
			return contents;
		}
		catch (IOException e)
		{
			throw new DoctorInFatalConditionException("Failed to read query file: " + getQueryFilePath(), e);
		}
	}
	
	private void clearQueryFile()
	{
		try
		{
			Files.write(queryFilePath, new byte[0]);
			log.debug("Cleared the temporary file: " + getQueryFilePath());
		}
		catch (IOException e)
		{
			throw new DoctorInFatalConditionException("Failed to properly delete temporary file: " + getQueryFilePath(), e);
		}
	}
}
