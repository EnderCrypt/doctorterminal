package endercrypt.drterminal.utility;


import java.util.ArrayList;
import java.util.List;

import lombok.Getter;


public class ListTraveller<T>
{
	private final ArrayList<T> items;
	
	@Getter
	private int index;
	
	public ListTraveller(List<T> elements)
	{
		this.items = new ArrayList<>(elements);
		this.index = -1;
		next();
	}
	
	public boolean next()
	{
		return travel(+1);
	}
	
	public boolean prev()
	{
		return travel(-1);
	}
	
	public int count()
	{
		return items.size();
	}
	
	public T getItem()
	{
		if (getIndex() == -1)
		{
			return null;
		}
		return items.get(getIndex());
	}
	
	private boolean travel(int direction)
	{
		if (Math.abs(direction) > 1)
		{
			throw new IllegalArgumentException("direction must be whitin -1 to 1, it cannot be " + direction);
		}
		int newIndex = getIndex() + direction;
		if (newIndex < 0 || newIndex >= count())
		{
			return false;
		}
		
		index = newIndex;
		return true;
	}
}
