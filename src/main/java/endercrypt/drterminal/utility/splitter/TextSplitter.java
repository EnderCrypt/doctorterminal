package endercrypt.drterminal.utility.splitter;


import endercrypt.library.commons.misc.Ender;

import java.util.Optional;
import java.util.stream.Stream;

import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class TextSplitter
{
	public static TextSplitter createStandard(String string)
	{
		TextSplitter splitter = new TextSplitter(string);
		splitter.getSoft().add(' ');
		splitter.getHard().add('\n');
		return splitter;
	}
	
	private final StringBuilder buffer;
	
	@Getter
	private final TextSplitterCharacters soft = new TextSplitterCharacters();
	
	@Getter
	private final TextSplitterCharacters hard = new TextSplitterCharacters();
	
	public TextSplitter(String string)
	{
		this(new StringBuilder(string));
	}
	
	public Optional<String> extract(int length)
	{
		if (hasMore() == false)
		{
			return Optional.empty();
		}
		int maxIndex = Ender.math.min(length, buffer.length());
		int hardIndex = hard.findSplittable(buffer.toString(), maxIndex);
		int softIndex = Math.min(hardIndex, soft.findSplittable(buffer.toString(), maxIndex));
		String result = buffer.substring(0, softIndex);
		buffer.delete(0, softIndex + 1);
		return Optional.of(result);
	}
	
	public boolean hasMore()
	{
		return buffer.length() > 0;
	}
	
	public Stream<String> stream(int length)
	{
		return Stream.generate(() -> extract(length))
			.takeWhile(Optional::isPresent)
			.flatMap(Optional::stream);
	}
	
}
