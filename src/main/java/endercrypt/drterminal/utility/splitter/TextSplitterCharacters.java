package endercrypt.drterminal.utility.splitter;


import java.util.HashSet;
import java.util.Set;


public class TextSplitterCharacters
{
	private final Set<Character> characters = new HashSet<>();
	
	public TextSplitterCharacters(char... chars)
	{
		for (char c : chars)
		{
			add(c);
		}
	}
	
	public void add(char c)
	{
		characters.add(c);
	}
	
	public int findSplittable(String text, int end)
	{
		if (end == text.length())
		{
			return end;
		}
		int index = end;
		while (true)
		{
			if (isSplitter(text, index))
			{
				return index;
			}
			if (index <= 0)
			{
				return end;
			}
			index--;
		}
	}
	
	public boolean isSplitter(String text, int index)
	{
		char c = text.charAt(index);
		return characters.contains(c);
	}
}
