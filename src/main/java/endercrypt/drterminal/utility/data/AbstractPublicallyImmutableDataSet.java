package endercrypt.drterminal.utility.data;


import java.util.HashSet;
import java.util.Set;


public class AbstractPublicallyImmutableDataSet<T> extends AbstractPublicallyImmutableDataCollection<Set<T>, T>
{
	@Override
	protected final Set<T> createCollection()
	{
		return new HashSet<>();
	}
	
	@Override
	public final Set<T> getAll()
	{
		return Set.copyOf(elements);
	}
}
