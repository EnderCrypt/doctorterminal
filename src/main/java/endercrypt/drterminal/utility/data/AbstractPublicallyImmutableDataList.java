package endercrypt.drterminal.utility.data;


import java.util.ArrayList;
import java.util.List;


public class AbstractPublicallyImmutableDataList<T> extends AbstractPublicallyImmutableDataCollection<List<T>, T>
{
	@Override
	protected final List<T> createCollection()
	{
		return new ArrayList<>();
	}
	
	@Override
	public final List<T> getAll()
	{
		return List.copyOf(elements);
	}
}
