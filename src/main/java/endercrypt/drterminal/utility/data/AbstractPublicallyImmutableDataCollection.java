package endercrypt.drterminal.utility.data;


import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Stream;


public abstract class AbstractPublicallyImmutableDataCollection<C extends Collection<T>, T> implements Iterable<T>
{
	protected final C elements = createCollection();
	
	protected abstract C createCollection();
	
	public abstract C getAll();
	
	public final Stream<T> stream()
	{
		return getAll().stream();
	}
	
	@Override
	public final Iterator<T> iterator()
	{
		return getAll().iterator();
	}
	
	@Override
	public String toString()
	{
		return elements.toString();
	}
}
