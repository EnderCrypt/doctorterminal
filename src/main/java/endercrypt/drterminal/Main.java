package endercrypt.drterminal;


import endercrypt.drterminal.arguments.MainArguments;
import endercrypt.drterminal.exceptions.DoctorInCriticalConditionException;
import endercrypt.drterminal.exceptions.DoctorInFatalConditionException;
import endercrypt.drterminal.sequences.ailment.directories.AilmentDirectories;
import endercrypt.drterminal.sequences.ailment.model.AilmentFactory;
import endercrypt.drterminal.sequences.ailment.repository.AilmentRepository;
import endercrypt.drterminal.sequences.doctor.Diagnosis;
import endercrypt.drterminal.sequences.doctor.Doctor;
import endercrypt.drterminal.sequences.symptom.Symptom;
import endercrypt.drterminal.sequences.ui.screens.UiScreen;
import endercrypt.drterminal.sequences.ui.screens.diagnosis.DiagnosisScreen;
import endercrypt.drterminal.sequences.ui.terminal.TerminalInterface;
import endercrypt.library.groundwork.information.Information;
import endercrypt.library.groundwork.launcher.PlainApplication;

import lombok.extern.log4j.Log4j2;


@Log4j2
public class Main extends PlainApplication<MainArguments>
{
	public static void main(String[] args)
	{
		PlainApplication.launch(Main.class, args);
	}
	
	private TerminalInterface terminal = null;
	
	@Override
	public void main(MainArguments arguments, Information information) throws Exception
	{
		try
		{
			try
			{
				log.info("Started " + information.getName() + " v" + information.getVersion() + " by " + information.getAuthor());
				protectedMain(arguments, information);
				System.exit(0);
			}
			catch (DoctorInFatalConditionException e)
			{
				TerminalInterface.disgracefulClose(terminal);
				throw e;
			}
		}
		catch (DoctorInCriticalConditionException e)
		{
			log.error(information.getName() + " crashed with a critical condition", e);
			System.out.println(e.getMessage());
		}
		catch (DoctorInFatalConditionException e)
		{
			log.error(information.getName() + " crashed with a fatal condition", e);
			System.out.println("The doctor has suffered a fatal accident");
			e.printStackTrace();
		}
		catch (Exception e)
		{
			log.error(information.getName() + " crashed with a fatal exception", e);
			System.out.println("The doctor has suffered a fatal exception");
			e.printStackTrace();
		}
		System.exit(1);
	}
	
	public void protectedMain(MainArguments arguments, Information information) throws DoctorInFatalConditionException
	{
		AilmentFactory factory = new AilmentFactory();
		
		AilmentDirectories directories = new AilmentDirectories();
		
		AilmentRepository repository = new AilmentRepository(factory, directories);
		
		Doctor doctor = new Doctor(repository);
		
		Symptom symptom = Symptom.askUser();
		
		Diagnosis diagnosis = doctor.diagnose(symptom);
		
		terminal = new TerminalInterface();
		
		UiScreen screen = new DiagnosisScreen(diagnosis);
		
		terminal.render(screen);
	}
}
